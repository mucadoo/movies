#!/bin/bash

# Wait for the PostgreSQL server to be ready
until nc -z postgres $DB_PORT; do
  echo "Waiting for PostgreSQL..."
  sleep 1
done

# Run migrations
npx sequelize-cli db:migrate

# Check if the SQL script has already been executed
if [ ! -f /usr/src/app/server/db/inserts.sql.lock ]; then
  # Execute SQL script after migrations
  PGPASSWORD=$DB_PASS psql -h $DB_HOST -U $DB_USER -d $DB_NAME -f /usr/src/app/server/db/inserts.sql

  # Create lock file to indicate the script has been run
  touch /usr/src/app/server/db/inserts.sql.lock
else
  echo "SQL script has already been executed. Skipping..."
fi

# Start the application
exec node dist/src/server.js
