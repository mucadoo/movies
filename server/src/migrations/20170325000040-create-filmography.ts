import { QueryInterface, DataTypes } from 'sequelize';

export default {
    up: async (queryInterface: QueryInterface): Promise<void> => {
        await queryInterface.createTable('filmographies', {
            movieId: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                references: {
                    model: 'movies',
                    key: 'id',
                },
                field: 'movie_id'
            },
            personId: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                references: {
                    model: 'people',
                    key: 'id',
                },
                field: 'person_id'
            },
            filmographyTypeId: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                references: {
                    model: 'filmography_types',
                    key: 'id',
                },
                field: 'filmography_type_id'
            },
            createdAt: {
                allowNull: false,
                type: DataTypes.DATE,
                field: 'created_at'
            },
            updatedAt: {
                allowNull: false,
                type: DataTypes.DATE,
                field: 'updated_at'
            }
        });
    },
    down: async (queryInterface: QueryInterface): Promise<void> => {
        await queryInterface.dropTable('filmographies');
    }
};
