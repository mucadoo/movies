import { QueryInterface, DataTypes } from 'sequelize';

export default {
    up: async (queryInterface: QueryInterface): Promise<void> => {
        await queryInterface.createTable('movies', {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            name: {
                type: DataTypes.STRING,
            },
            year: {
                type: DataTypes.INTEGER,
            },
            running_time: {
                type: DataTypes.INTEGER,
            },
            poster_link: {
                type: DataTypes.STRING,
            },
            release_date: {
                type: DataTypes.DATE,
            },
            summary_text: {
                type: DataTypes.TEXT,
            },
            storyline: {
                type: DataTypes.TEXT,
            },
            country: {
                type: DataTypes.STRING,
            },
            language: {
                type: DataTypes.STRING,
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: false,
            },
            updated_at: {
                type: DataTypes.DATE,
                allowNull: false,
            },
        });
    },
    down: async (queryInterface: QueryInterface): Promise<void> => {
        await queryInterface.dropTable('movies');
    }
};
