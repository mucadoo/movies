import { QueryInterface, DataTypes } from 'sequelize';

export default {
    up: async (queryInterface: QueryInterface): Promise<void> => {
        await queryInterface.createTable('movie_genres', {
            movieId: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                references: {
                    model: 'movies',
                    key: 'id',
                },
                field: 'movie_id'
            },
            genreId: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                references: {
                    model: 'genres',
                    key: 'id',
                },
                field: 'genre_id'
            },
            createdAt: {
                allowNull: false,
                type: DataTypes.DATE,
                field: 'created_at'
            },
            updatedAt: {
                allowNull: false,
                type: DataTypes.DATE,
                field: 'updated_at'
            }
        });
    },
    down: async (queryInterface: QueryInterface): Promise<void> => {
        await queryInterface.dropTable('movie_genres');
    }
};
