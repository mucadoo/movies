import { Table, Column, Model, DataType, BelongsToMany } from 'sequelize-typescript';
import Movie from './Movie';
import MovieGenre from './MovieGenre';

@Table({
    tableName: 'genres',
    underscored: true,
})
class Genre extends Model<Genre> {
    @Column({
        type: DataType.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    })
    id!: number;

    @Column(DataType.STRING)
    name!: string;

    @BelongsToMany(() => Movie, () => MovieGenre)
    movies!: Movie[];
}

export default Genre;
