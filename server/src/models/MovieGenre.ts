import { Table, Column, Model, DataType, ForeignKey } from 'sequelize-typescript';
import Movie from './Movie';
import Genre from './Genre';

@Table({
    tableName: 'movie_genres',
    underscored: true,
})
class MovieGenre extends Model<MovieGenre> {
    @ForeignKey(() => Movie)
    @Column({
        type: DataType.INTEGER,
        primaryKey: true,
    })
    movieId!: number;

    @ForeignKey(() => Genre)
    @Column({
        type: DataType.INTEGER,
        primaryKey: true,
    })
    genreId!: number;
}

export default MovieGenre;
