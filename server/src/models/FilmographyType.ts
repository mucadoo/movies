import {Table, Column, Model, DataType, HasMany, BelongsToMany} from 'sequelize-typescript';
import Filmography from './Filmography';
import Movie from "./Movie";
import Person from "./Person";

@Table({
    tableName: 'filmography_types',
    underscored: true,
})
class FilmographyType extends Model<FilmographyType> {
    @Column({
        type: DataType.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    })
    id!: number;

    @Column(DataType.STRING)
    name!: string;

    @BelongsToMany(() => Movie, () => Filmography)
    movies!: Movie[];

    @BelongsToMany(() => Person, () => Filmography)
    people!: Person[];
}

export default FilmographyType;
