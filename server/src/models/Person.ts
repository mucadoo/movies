import { Table, Column, Model, DataType, BelongsToMany, HasMany } from 'sequelize-typescript';
import Movie from './Movie';
import Filmography from './Filmography';
import FilmographyType from './FilmographyType';

@Table({
    tableName: 'people',
    underscored: true,
})
class Person extends Model<Person> {
    @Column({
        type: DataType.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    })
    id!: number;

    @Column(DataType.STRING)
    name!: string;

    @Column(DataType.STRING)
    posterLink!: string;

    @Column(DataType.TEXT)
    resume!: string;

    @Column(DataType.DATE)
    born!: Date;

    @BelongsToMany(() => Movie, () => Filmography)
    movies!: Movie[];

    @BelongsToMany(() => FilmographyType, () => Filmography)
    filmographyTypes!: FilmographyType[];

    @HasMany(() => Filmography)
    filmographies!: Filmography[];

}

export default Person;
