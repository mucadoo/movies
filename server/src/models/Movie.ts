import { Table, Column, Model, DataType, BelongsToMany, HasMany } from 'sequelize-typescript';
import Genre from './Genre';
import Person from './Person';
import Filmography from './Filmography';
import MovieGenre from './MovieGenre';
import Vote from './Vote';
import FilmographyType from './FilmographyType';

@Table({
    tableName: 'movies',
    underscored: true,
})
class Movie extends Model<Movie> {
    @Column({
        type: DataType.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    })
    id!: number;

    @Column(DataType.STRING)
    name!: string;

    @Column(DataType.INTEGER)
    year!: number;

    @Column(DataType.INTEGER)
    runningTime!: number;

    @Column(DataType.STRING)
    posterLink!: string;

    @Column(DataType.DATE)
    releaseDate!: Date;

    @Column(DataType.TEXT)
    summaryText!: string;

    @Column(DataType.TEXT)
    storyline!: string;

    @Column(DataType.STRING)
    country!: string;

    @Column(DataType.STRING)
    language!: string;

    @BelongsToMany(() => Genre, () => MovieGenre)
    genres!: Genre[];

    @BelongsToMany(() => Person, () => Filmography)
    people!: Person[];

    @BelongsToMany(() => FilmographyType, () => Filmography)
    filmographyTypes!: FilmographyType[];

    @HasMany(() => Filmography)
    filmographies!: Filmography[];

    @HasMany(() => Vote)
    votes!: Vote[];
}

export default Movie;
