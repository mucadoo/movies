import { Table, Column, Model, DataType, ForeignKey, BelongsTo } from 'sequelize-typescript';
import Movie from './Movie';
import Person from './Person';
import FilmographyType from './FilmographyType';

@Table({
    tableName: 'filmographies',
    underscored: true,
})
class Filmography extends Model<Filmography> {
    @ForeignKey(() => Movie)
    @Column({
        type: DataType.INTEGER,
        primaryKey: true,
    })
    movieId!: number;

    @ForeignKey(() => Person)
    @Column({
        type: DataType.INTEGER,
        primaryKey: true,
    })
    personId!: number;

    @ForeignKey(() => FilmographyType)
    @Column({
        type: DataType.INTEGER,
        primaryKey: true,
    })
    filmographyTypeId!: number;

    @BelongsTo(() => Movie)
    movie!: Movie;

    @BelongsTo(() => Person)
    person!: Person;

    @BelongsTo(() => FilmographyType)
    filmographyType!: FilmographyType;
}

export default Filmography;
