import { Sequelize } from 'sequelize-typescript';
import * as dotenv from 'dotenv';
import Movie from './Movie';
import Person from './Person';
import Vote from './Vote';
import Genre from './Genre';
import MovieGenre from './MovieGenre';
import Filmography from './Filmography';
import FilmographyType from './FilmographyType';

dotenv.config();

const sequelize = new Sequelize({
    database: process.env.DB_NAME as string,
    dialect: 'postgres',
    username: process.env.DB_USER as string,
    password: process.env.DB_PASS as string,
    host: process.env.DB_HOST as string,
    port: Number(process.env.DB_PORT),
    models: [Movie, Person, Vote, Genre, MovieGenre, Filmography, FilmographyType],
});

// Export the models for use in other parts of the application
export { sequelize, Movie, Person, Vote, Genre, MovieGenre, Filmography, FilmographyType };
