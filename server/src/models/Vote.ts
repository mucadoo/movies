import { Table, Column, Model, DataType, ForeignKey, BelongsTo } from 'sequelize-typescript';
import Movie from './Movie';

@Table({
    tableName: 'votes',
    underscored: true,
})
class Vote extends Model<Vote> {
    @Column({
        type: DataType.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    })
    id!: number;

    @Column(DataType.STRING)
    email!: string;

    @Column(DataType.INTEGER)
    grade!: number;

    @ForeignKey(() => Movie)
    @Column(DataType.INTEGER)
    movieId!: number;

    @BelongsTo(() => Movie)
    movie!: Movie;
}

export default Vote;
