import { Request, Response } from 'express';
import Vote from '../models/Vote';

// Get all votes for a specific movie
export const getVotesByMovie = async (req: Request, res: Response): Promise<void> => {
    try {
        const { movieId } = req.params;
        const votes = await Vote.findAll({ where: { movieId } });
        res.status(200).json(votes);
    } catch (err) {
        res.status(500).json({ error: (err as Error).message });
    }
};

// Create or update a vote based on movie and email
export const createOrUpdateVote = async (req: Request, res: Response): Promise<void> => {
    try {
        const { email, movieId, grade } = req.body;
        let vote = await Vote.findOne({
            where: { email, movieId },
        });
        if (vote) {
            vote.grade = grade;
            await vote.save();
            res.status(200).json(vote);
        } else {
            vote = await Vote.create(req.body);
            res.status(201).json(vote);
        }
    } catch (err) {
        res.status(500).json({ error: (err as Error).message });
    }
};