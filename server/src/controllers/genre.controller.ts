import { Request, Response } from 'express';
import Genre from '../models/Genre';

// Get all genres
export const getGenres = async (req: Request, res: Response): Promise<void> => {
    try {
        const genres = await Genre.findAll();
        res.status(200).json(genres);
    } catch (err) {
        res.status(500).json({ error: (err as Error).message });
    }
};
