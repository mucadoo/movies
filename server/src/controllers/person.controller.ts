import { Request, Response } from 'express';
import { Op } from 'sequelize';
import Person from '../models/Person';
import Movie from '../models/Movie';
import Filmography from '../models/Filmography';
import {FilmographyType} from "../models";

// Get all people or search people by query
export const getPeople = async (req: Request, res: Response): Promise<void> => {
    try {
        const { query } = req.query as { query?: string };
        let whereClause = {};

        if (query && query.trim()) {
            whereClause = {
                [Op.or]: [
                    { name: { [Op.iLike]: `%${query}%` } },
                    { resume: { [Op.iLike]: `%${query}%` } }
                ],
            };
        }

        const people = await Person.findAll({
            where: whereClause
        });
        res.status(200).json(people);
    } catch (err) {
        res.status(500).json({ error: (err as Error).message });
    }
};

// Get a person by ID
export const getPersonById = async (req: Request, res: Response): Promise<void> => {
    try {
        const { id } = req.params;
        const person = await Person.findByPk(id, {
            include: [{ model: Filmography, include: [Movie, FilmographyType] }]
        });

        if (!person) {
            res.status(404).json({ error: 'Person not found' });
            return;
        }

        res.status(200).json(person);
    } catch (err) {
        res.status(500).json({ error: (err as Error).message });
    }
};

// Create a new person
export const createPerson = async (req: Request, res: Response): Promise<void> => {
    try {
        const { name, posterLink, resume, born, filmographies } = req.body;
        const person = await Person.create({ name, posterLink, resume, born } as any);

        if (filmographies) {
            const filmographiesData = filmographies.map((filmography: { movie: { id: number }; filmographyType: { id: number } }) => ({
                movieId: filmography.movie.id,
                personId: person.id,
                filmographyTypeId: filmography.filmographyType.id
            }));
            await Filmography.bulkCreate(filmographiesData);
        }

        res.status(201).json(person);
    } catch (err) {
        res.status(500).json({ error: (err as Error).message });
    }
};

// Update a person
export const updatePerson = async (req: Request, res: Response): Promise<void> => {
    try {
        const { id } = req.params;
        const { name, posterLink, resume, born, filmographies } = req.body;
        const person = await Person.findByPk(id);

        if (!person) {
            res.status(404).json({ error: 'Person not found' });
            return;
        }

        await person.update({ name, posterLink, resume, born });

        if (filmographies) {
            await Filmography.destroy({ where: { personId: person.id } }); // Remove existing Filmography entries

            const filmographiesData = filmographies.map((filmography: { movie: { id: number }; filmographyType: { id: number } }) => ({
                movieId: filmography.movie.id,
                personId: person.id,
                filmographyTypeId: filmography.filmographyType.id
            }));
            await Filmography.bulkCreate(filmographiesData); // Create new Filmography entries
        }

        res.status(200).json(person);
    } catch (err) {
        res.status(500).json({ error: (err as Error).message });
    }
};

// Delete a person
export const deletePerson = async (req: Request, res: Response): Promise<void> => {
    try {
        const { id } = req.params;
        const person = await Person.findByPk(id);

        if (!person) {
            res.status(404).json({ error: 'Person not found' });
            return;
        }

        // Manually delete associated Filmography entries
        await Filmography.destroy({ where: { personId: id } });

        // Finally, delete the person
        await person.destroy();

        res.status(204).send();
    } catch (err) {
        res.status(500).json({ error: (err as Error).message });
    }
};
