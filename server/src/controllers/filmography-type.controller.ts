import { Request, Response } from 'express';
import FilmographyType from '../models/FilmographyType';

// Get all filmography types
export const getFilmographyTypes = async (req: Request, res: Response): Promise<void> => {
    try {
        const types = await FilmographyType.findAll();
        res.status(200).json(types);
    } catch (err) {
        res.status(500).json({ error: (err as Error).message });
    }
};
