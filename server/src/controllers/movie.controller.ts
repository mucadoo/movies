import { Request, Response } from 'express';
import { Op } from 'sequelize';
import Movie from '../models/Movie';
import Genre from '../models/Genre';
import Filmography from '../models/Filmography';
import {FilmographyType, MovieGenre, Person, Vote} from '../models';

// Get all movies or search movies by query
export const getMovies = async (req: Request, res: Response): Promise<void> => {
    try {
        const { query } = req.query as { query?: string };
        let whereClause = {};

        if (query && query.trim()) {
            whereClause = {
                [Op.or]: [
                    { name: { [Op.iLike]: `%${query}%` } },
                    { summaryText: { [Op.iLike]: `%${query}%` } },
                    { storyline: { [Op.iLike]: `%${query}%` } }
                ],
            };
        }

        const movies = await Movie.findAll({
            where: whereClause
        });
        res.status(200).json(movies);
    } catch (err) {
        console.error('Error fetching movies:', err);
        res.status(500).json({ error: (err as Error).message });
    }
};

// Get a movie by ID
export const getMovieById = async (req: Request, res: Response): Promise<void> => {
    try {
        const { id } = req.params;
        const movie = await Movie.findByPk(id, {
            include: [Genre, Vote, { model: Filmography, include: [Person, FilmographyType] }]
        });

        if (!movie) {
            res.status(404).json({ error: 'Movie not found' });
            return;
        }

        res.status(200).json(movie);
    } catch (err) {
        console.error('Error fetching movie:', err);
        res.status(500).json({ error: (err as Error).message });
    }
};

// Create a new movie
export const createMovie = async (req: Request, res: Response): Promise<void> => {
    try {
        const {
            name,
            year,
            runningTime,
            posterLink,
            releaseDate,
            summaryText,
            storyline,
            country,
            language,
            genres,
            filmographies
        } = req.body;
        const movie = await Movie.create({
            name,
            year,
            runningTime,
            posterLink,
            releaseDate,
            summaryText,
            storyline,
            country,
            language
        } as any);

        if (genres) {
            const genreIds = genres.map((genre: { id: number }) => genre.id);
            await movie.$set('genres', genreIds);
        }

        if (filmographies) {
            const filmographiesData = filmographies.map((filmography: { person: { id: number }, filmographyType: { id: number } }) => ({
                movieId: movie.id,
                personId: filmography.person.id,
                filmographyTypeId: filmography.filmographyType.id
            }));
            await Filmography.bulkCreate(filmographiesData);
        }

        res.status(201).json(movie);
    } catch (err) {
        res.status(500).json({ error: (err as Error).message });
    }
};

// Update a movie
export const updateMovie = async (req: Request, res: Response): Promise<void> => {
    try {
        const { id } = req.params;
        const {
            name,
            year,
            runningTime,
            posterLink,
            releaseDate,
            summaryText,
            storyline,
            country,
            language,
            genres,
            filmographies
        } = req.body;
        const movie = await Movie.findByPk(id);

        if (!movie) {
            res.status(404).json({ error: 'Movie not found' });
            return;
        }

        await movie.update({
            name,
            year,
            runningTime,
            posterLink,
            releaseDate,
            summaryText,
            storyline,
            country,
            language
        });

        if (genres) {
            const genreIds = genres.map((genre: { id: number }) => genre.id);
            await movie.$set('genres', []);
            await movie.$set('genres', genreIds);
        }

        if (filmographies) {
            await Filmography.destroy({ where: { movieId: movie.id } });

            const filmographiesData = filmographies.map((filmography: { person: { id: number }, filmographyType: { id: number } }) => ({
                movieId: movie.id,
                personId: filmography.person.id,
                filmographyTypeId: filmography.filmographyType.id
            }));
            await Filmography.bulkCreate(filmographiesData);
        }

        res.status(200).json(movie);
    } catch (err) {
        res.status(500).json({ error: (err as Error).message });
    }
};

// Delete a movie
export const deleteMovie = async (req: Request, res: Response): Promise<void> => {
    try {
        const { id } = req.params;
        const movie = await Movie.findByPk(id);

        if (!movie) {
            res.status(404).json({ error: 'Movie not found' });
            return;
        }

        // Remove associated genres
        await movie.$set('genres', []); // Disassociate all genres

        // Manually delete associated Filmography entries
        await Filmography.destroy({ where: { movieId: id } });

        // Manually delete associated MovieGenre entries
        await MovieGenre.destroy({ where: { movieId: id } });

        // Manually delete associated Vote entries
        await Vote.destroy({ where: { movieId: id } });

        // Finally, delete the movie
        await movie.destroy();

        res.status(204).send();
    } catch (err) {
        res.status(500).json({ error: (err as Error).message });
    }
};
