import * as dotenv from 'dotenv';
import { Dialect } from 'sequelize';

dotenv.config();

interface SequelizeConfig {
    username: string;
    password: string;
    database: string;
    host: string;
    port: number;
    dialect: Dialect;
}

interface Config {
    development: SequelizeConfig;
    test: SequelizeConfig;
    production: SequelizeConfig;
}

const config: Config = {
    development: {
        username: process.env.DB_USER as string,
        password: process.env.DB_PASS as string,
        database: process.env.DB_NAME as string,
        host: process.env.DB_HOST as string,
        port: parseInt(process.env.DB_PORT as string, 10),
        dialect: 'postgres',
    },
    test: {
        username: process.env.DB_USER as string,
        password: process.env.DB_PASS as string,
        database: process.env.DB_NAME as string,
        host: process.env.DB_HOST as string,
        port: parseInt(process.env.DB_PORT as string, 10),
        dialect: 'postgres',
    },
    production: {
        username: process.env.DB_USER as string,
        password: process.env.DB_PASS as string,
        database: process.env.DB_NAME as string,
        host: process.env.DB_HOST as string,
        port: parseInt(process.env.DB_PORT as string, 10),
        dialect: 'postgres',
    },
};

module.exports = config;