import { Router } from 'express';
import { getFilmographyTypes } from '../controllers/filmography-type.controller';

const router = Router();

router.get('/', getFilmographyTypes);

export default router;
