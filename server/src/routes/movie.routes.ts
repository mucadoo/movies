import express = require('express');
import {getMovies, getMovieById, createMovie, updateMovie, deleteMovie} from '../controllers/movie.controller';
import {validateRequest} from "../middleware/validation";
import {movieSchema} from "../validations/movie.validation";

const router = express.Router();

router.get('/', getMovies);
router.get('/:id', getMovieById);
router.post('/', validateRequest(movieSchema), createMovie);
router.put('/:id', validateRequest(movieSchema), updateMovie);
router.delete('/:id', deleteMovie);

export default router;
