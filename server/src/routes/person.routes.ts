import express from 'express';
import {getPeople, getPersonById, createPerson, updatePerson, deletePerson} from '../controllers/person.controller';
import {validateRequest} from '../middleware/validation';
import {personSchema} from '../validations/person.validation';

const router = express.Router();

router.get('/', getPeople);
router.get('/:id', getPersonById);
router.post('/', validateRequest(personSchema), createPerson);
router.put('/:id', validateRequest(personSchema), updatePerson);
router.delete('/:id', deletePerson);

export default router;
