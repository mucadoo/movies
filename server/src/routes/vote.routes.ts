import express from 'express';
import {createOrUpdateVote, getVotesByMovie} from '../controllers/vote.controller';
import {validateRequest} from '../middleware/validation';
import {voteSchema} from '../validations/vote.validation';

const router = express.Router();

router.get('/movie/:movieId', getVotesByMovie);
router.post('/', validateRequest(voteSchema), createOrUpdateVote);

export default router;
