import { ObjectSchema } from 'joi';
import { Request, Response, NextFunction } from 'express';

export const validateRequest = (schema: ObjectSchema) => {
    return (req: Request, res: Response, next: NextFunction): void => {
        const { error } = schema.validate(req.body);
        if (error) {
            res.status(400).json({ error: error.details[0].message });
        } else {
            next();
        }
    };
};
