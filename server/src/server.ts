import * as dotenv from 'dotenv';
import express, { Application } from 'express';
import cors from 'cors';
import { sequelize } from './models';
import movieRoutes from './routes/movie.routes';
import personRoutes from './routes/person.routes';
import voteRoutes from './routes/vote.routes';
import filmographyTypeRoutes from './routes/filmography-type.routes';
import genreRoutes from './routes/genre.routes';
import winston from 'winston';

dotenv.config();

const app: Application = express();

// Use CORS middleware
app.use(cors({
    origin: 'http://localhost:4200', // Allow requests from this origin
}));

app.use(express.json());
app.use('/api/movies', movieRoutes);
app.use('/api/people', personRoutes);
app.use('/api/votes', voteRoutes);
app.use('/api/filmography-types', filmographyTypeRoutes);
app.use('/api/genres', genreRoutes);

// Logger setup
const logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        winston.format.printf(({ timestamp, level, message }) => `${timestamp} ${level}: ${message}`)
    ),
    transports: [new winston.transports.Console()]
});

const startServer = async (): Promise<void> => {
    try {
        await sequelize.authenticate();
        logger.info('Database connected!');

        app.listen(process.env.APP_PORT, () => {
            logger.info(`Server is running on port ${process.env.APP_PORT}`);
        });
    } catch (err) {
        logger.error('Unable to connect to the database:', err);
    }
};

startServer();
