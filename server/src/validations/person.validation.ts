import Joi from 'joi';

export const personSchema = Joi.object({
    name: Joi.string().required(),
    posterLink: Joi.string().uri().required(),
    resume: Joi.string().required(),
    born: Joi.date().required(),
    filmographies: Joi.array().items(
        Joi.object({
            movie: Joi.object({
                id: Joi.number().integer().required(),
                name: Joi.string()
            }).unknown().required(),
            filmographyType: Joi.object({
                id: Joi.number().integer().required(),
                name: Joi.string()
            }).unknown().required()
        }).unknown()
    ).required()
}).unknown();