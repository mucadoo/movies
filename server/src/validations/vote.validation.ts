import Joi from 'joi';

export const voteSchema = Joi.object({
    email: Joi.string().email().required(),
    movieId: Joi.number().integer().required(),
    grade: Joi.number().integer().min(1).max(10).required()
});
