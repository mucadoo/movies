import Joi from 'joi';

export const movieSchema = Joi.object({
    name: Joi.string().required(),
    year: Joi.number().integer().required(),
    runningTime: Joi.number().integer().required(),
    posterLink: Joi.string().uri().required(),
    releaseDate: Joi.date().required(),
    summaryText: Joi.string().required(),
    storyline: Joi.string().required(),
    country: Joi.string().required(),
    language: Joi.string().required(),
    genres: Joi.array().items(
        Joi.object({
            id: Joi.number().integer().required(),
            name: Joi.string()
        }).unknown()
    ).min(1).required(),
    filmographies: Joi.array().items(
        Joi.object({
            person: Joi.object({
                id: Joi.number().integer().required(),
                name: Joi.string()
            }).unknown().required(),
            filmographyType: Joi.object({
                id: Joi.number().integer().required(),
                name: Joi.string()
            }).unknown().required()
        }).unknown()
    )
}).unknown();
