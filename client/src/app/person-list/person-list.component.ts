import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgForOf } from '@angular/common';
import { PersonService } from '../services/person.service';
import { Person } from '../model/person.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PersonModalComponent } from '../person-modal/person-modal.component';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss'],
  imports: [
    FormsModule,
    NgForOf
  ]
})
export class PersonListComponent implements OnInit {
  searchPerson: string = '';
  people: Person[] = [];
  selectedPersonId: string | null = null;

  constructor(private personService: PersonService, private router: Router, private modalService: NgbModal) {}

  ngOnInit(): void {
    this.updatePersonList();
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.selectedPersonId = this.getCurrentPersonId();
      }
    });

    this.personService.personUpdated$.subscribe(() => {
      this.updatePersonList();
    });
  }

  updatePersonList(): void {
    this.personService.getPeople(this.searchPerson).subscribe(data => {
      this.people = data;
      this.selectedPersonId = this.getCurrentPersonId();
    });
  }

  showPerson(person: any): void {
    this.router.navigate(['people', person.id]);
  }

  getCurrentPersonId(): string | null {
    const currentUrl = this.router.routerState.snapshot.url;
    const urlParts = currentUrl.split('/');
    return currentUrl.startsWith('/people/') && urlParts.length === 3 ? urlParts[2] : null;
  }

  isSelected(person: Person): boolean {
    return this.selectedPersonId != null && person.id != null && this.selectedPersonId === person.id.toString();
  }

  addPerson(): void {
    const modalRef = this.modalService.open(PersonModalComponent, { size: 'xl' });
    modalRef.componentInstance.isEditMode = false;
    modalRef.componentInstance.person = {} as Person;
  }
}
