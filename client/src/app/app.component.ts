import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, NavigationEnd, RouterLinkActive, RouterLink} from '@angular/router';
import { filter } from 'rxjs/operators';
import { NgIf } from '@angular/common';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { PersonDetailComponent } from './person-detail/person-detail.component';
import { MovieListComponent } from './movie-list/movie-list.component';
import { PersonListComponent } from './person-list/person-list.component';
import {ToastContainerComponent} from './toast-container/toast-container.component';
import {LoaderComponent} from './loader/loader.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  imports: [
    NgIf,
    MovieDetailComponent,
    PersonDetailComponent,
    MovieListComponent,
    PersonListComponent,
    RouterLinkActive,
    RouterLink,
    ToastContainerComponent,
    LoaderComponent
  ],
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  showMovieDetail: boolean = false;
  showPeopleDetail: boolean = false;
  showMovieList: boolean = false;
  showPeopleList: boolean = false;
  movieId: string | null = null;
  personId: string | null = null;

  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
      this.updateDetailState();
    });

  }

  private updateDetailState(): void {
    const currentUrl = this.router.routerState.snapshot.url;
    const urlParts = currentUrl.split('/');

    this.showMovieDetail = currentUrl.startsWith('/movies/') && urlParts.length === 3;
    this.showPeopleDetail = currentUrl.startsWith('/people/') && urlParts.length === 3;

    this.showMovieList = currentUrl.startsWith('/movies');
    this.showPeopleList = currentUrl.startsWith('/people');

    if (this.showMovieDetail) {
      this.movieId = urlParts[2];
    } else {
      this.movieId = null;
    }

    if (this.showPeopleDetail) {
      this.personId = urlParts[2];
    } else {
      this.personId = null;
    }

  }
}
