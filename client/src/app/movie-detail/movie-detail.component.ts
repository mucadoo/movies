import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RatingModalComponent } from '../rating-modal/rating-modal.component';
import { MovieService } from '../services/movie.service';
import { VoteService } from '../services/vote.service';
import { Movie } from '../model/movie.model';
import { FilmographyType } from '../model/filmography-type.model';
import { Filmography } from '../model/filmography.model';
import { DatePipe, NgForOf, NgIf } from '@angular/common';
import { MovieModalComponent } from '../movie-modal/movie-modal.component';
import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss'],
  imports: [
    NgForOf,
    NgIf,
    DatePipe
  ]
})
export class MovieDetailComponent implements OnInit, OnChanges {
  @Input() movieId: string | null = null;
  selectedMovie: Movie | undefined;
  averageGrade: number | null = null;
  activeTab: number = 0;

  constructor(
    private movieService: MovieService,
    private voteService: VoteService,
    private modalService: NgbModal,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadMovieDetail();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['movieId'] && !changes['movieId'].isFirstChange()) {
      this.loadMovieDetail();
    }
  }

  private loadMovieDetail(): void {
    if (this.movieId) {
      this.movieService.getMovie(this.movieId).subscribe(data => {
        this.selectedMovie = data;
        this.loadVotes();
      });
    }
  }

  private loadVotes(): void {
    if (this.movieId) {
      this.voteService.getVotesByMovie(this.movieId).subscribe(data => {
        if (this.selectedMovie) {
          this.selectedMovie.votes = data;
          this.calculateAverageGrade();
        }
      });
    }
  }

  private calculateAverageGrade(): void {
    if (this.selectedMovie && this.selectedMovie.votes && this.selectedMovie.votes.length > 0) {
      const totalGrade = this.selectedMovie.votes.reduce((sum, vote) => sum + vote.grade, 0);
      this.averageGrade = parseFloat((totalGrade / this.selectedMovie.votes.length).toFixed(1)); // Round to nearest tenth
    } else {
      this.averageGrade = null;
    }
  }

  getUniqueFilmographyTypes(): FilmographyType[] {
    if (!this.selectedMovie || !this.selectedMovie.filmographies) {
      return [];
    }
    const uniqueTypes = new Map<number, FilmographyType>();
    this.selectedMovie.filmographies.forEach(f => {
      if (f.filmographyType) {
        uniqueTypes.set(f.filmographyType.id, f.filmographyType);
      }
    });
    return Array.from(uniqueTypes.values());
  }

  getFilmographiesByType(typeId: number): Filmography[] {
    if (!this.selectedMovie || !this.selectedMovie.filmographies) {
      return [];
    }
    return this.selectedMovie.filmographies.filter(
      filmography => filmography.filmographyType?.id === typeId
    );
  }

  navigateToPerson(movieId: number | undefined) {
    this.router.navigate([`/people/${movieId}`]);
  }

  openRatingModal(): void {
    const modalRef = this.modalService.open(RatingModalComponent);
    if (this.selectedMovie) {
      modalRef.componentInstance.movieId = this.selectedMovie.id;
      modalRef.componentInstance.movieName = this.selectedMovie.name;
      modalRef.componentInstance.ratingSubmitted.subscribe(() => {
        this.loadVotes();
      });
    }
  }

  editMovie(editMode: boolean, movie: Movie = new Movie()): void {
    const modalRef = this.modalService.open(MovieModalComponent, { size: 'xl' });
    modalRef.componentInstance.isEditMode = editMode;
    modalRef.componentInstance.movie = movie;

    modalRef.result.then((result) => {
      if (result === 'saved') {
        this.loadMovieDetail();
      }
    }).catch((error) => {
      console.log(error);
    });
  }

  deleteMovie(movie: Movie): void {
    const modalRef = this.modalService.open(ConfirmationModalComponent);
    modalRef.componentInstance.message = `Are you sure you want to delete the movie "${movie.name}"?`;

    modalRef.result.then((result) => {
      if (result === 'confirm') {
        this.movieService.deleteMovie(String(movie.id)).subscribe(() => {
          this.movieService.notifyMovieUpdated();
          this.router.navigate(['/movies']);
        });
      }
    }).catch((error) => {
      console.log(error);
    });
  }
}
