export interface Vote {
  id?: number;
  email: string;
  grade: number;
  movieId: number;
  createdAt?: string;
  updatedAt?: string;
}
