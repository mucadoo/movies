import {Genre} from './genre.model';
import {Vote} from './vote.model';
import {Filmography} from './filmography.model';

export class Movie {
  id?: number;
  name: string = '';
  year?: number;
  posterLink: string = '';
  summaryText: string = '';
  storyline: string = '';
  country: string = '';
  language: string = '';
  releaseDate: string = '';
  runningTime?: number;
  createdAt?: string;
  updatedAt?: string;
  filmographies?: Filmography[];
  genres: Genre[] = [];
  votes?: Vote[];
}
