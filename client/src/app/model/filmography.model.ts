import {Person} from './person.model';
import {FilmographyType} from './filmography-type.model';
import {Movie} from './movie.model';

export interface Filmography {
  movieId?: number;
  personId?: number;
  filmographyTypeId?: number;
  createdAt?: string;
  updatedAt?: string;
  person?: Person;
  movie?: Movie;
  filmographyType?: FilmographyType;
}
