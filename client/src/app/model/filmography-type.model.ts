import {Filmography} from './filmography.model';

export interface FilmographyType {
  id: number;
  name: string;
  createdAt: string;
  updatedAt: string;
  Filmography: Filmography;
}
