import { Filmography } from './filmography.model';

export class Person {
  id?: number;
  name: string = '';
  posterLink: string = '';
  resume: string = '';
  born: string = '';
  createdAt?: string;
  updatedAt?: string;
  filmographies?: Filmography[];
}
