export interface Genre {
  id?: number;
  name: string;
  createdAt?: string;
  updatedAt?: string;
}
