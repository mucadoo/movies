import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidatorFn, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MovieService } from '../services/movie.service';
import { GenreService } from '../services/genre.service';
import { PersonService } from '../services/person.service';
import { FilmographyTypeService } from '../services/filmography-type.service';
import { Router } from '@angular/router';
import { Movie } from '../model/movie.model';
import { Genre } from '../model/genre.model';
import { Person } from '../model/person.model';
import { Filmography } from '../model/filmography.model';
import { FilmographyType } from '../model/filmography-type.model';
import { NgClass, NgForOf, NgIf } from '@angular/common';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ToastService } from '../services/toast.service';

function atLeastOneGenreValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const genres = control.value;
    return genres && genres.length > 0 ? null : { atLeastOne: true };
  };
}

function formatNgbDateStruct(dateString: string): { year: number, month: number, day: number } {
  const date = new Date(dateString);
  return {
    year: date.getFullYear(),
    month: date.getMonth() + 1,
    day: date.getDate()
  };
}

function deepCopyAndFormat<T>(object: T): T {
  const copy = JSON.parse(JSON.stringify(object));
  if (copy && copy.releaseDate) {
    copy.releaseDate = formatNgbDateStruct(copy.releaseDate);
  }
  return copy;
}

function convertNgbDateToISO(ngbDate: { year: number, month: number, day: number }): string {
  const date = new Date(Date.UTC(ngbDate.year, ngbDate.month - 1, ngbDate.day));
  return date.toISOString();
}

@Component({
  selector: 'app-movie-modal',
  templateUrl: './movie-modal.component.html',
  imports: [
    ReactiveFormsModule,
    NgClass,
    NgMultiSelectDropDownModule,
    FormsModule,
    NgIf,
    NgForOf,
    NgbInputDatepicker
  ],
  styleUrls: ['./movie-modal.component.scss']
})
export class MovieModalComponent implements OnInit {
  @Input() isEditMode: boolean = false;
  @Input() movie: Movie = new Movie();
  editedMovie: Movie = new Movie();
  genres: Genre[] = [];
  filmographyTypes: FilmographyType[] = [];
  people: Person[] = [];
  selectedFilmographyType: FilmographyType | null = null;
  selectedPerson: Person | null = null;
  showValidation: boolean = false;
  movieForm: FormGroup;
  dropdownSettings = {};
  peopleDropdownSettings = {};
  filmographyTypeDropdownSettings = {};

  constructor(
    public activeModal: NgbActiveModal,
    private movieService: MovieService,
    private genreService: GenreService,
    private personService: PersonService,
    private filmographyTypeService: FilmographyTypeService,
    private fb: FormBuilder,
    private router: Router,
    protected toastService: ToastService,
    private el: ElementRef
  ) {
    this.movieForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      year: ['', [Validators.required, Validators.min(1888), Validators.max(2100)]],
      releaseDate: ['', Validators.required],
      runningTime: ['', [Validators.required, Validators.min(1), Validators.max(600)]],
      country: ['', [Validators.required, Validators.maxLength(50)]],
      language: ['', [Validators.required, Validators.maxLength(50)]],
      posterLink: ['', [Validators.required, Validators.pattern(/^(ftp|http|https):\/\/[^ "]+$/)]],
      summaryText: ['', [Validators.required, Validators.maxLength(500)]],
      storyline: ['', [Validators.required, Validators.maxLength(1000)]],
      genres: [[], atLeastOneGenreValidator()]
    });
  }

  ngOnInit(): void {
    // Create a deep copy of the movie object for editing
    this.editedMovie = deepCopyAndFormat(this.movie);

    // Load genres and filmography types
    this.loadGenres();
    this.loadFilmographyTypes();

    // Patch the movie form with the edited movie data
    this.movieForm.patchValue(this.editedMovie);

    this.people = [];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection: false,
      enableCheckAll: false
    };

    this.peopleDropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      allowSearchFilter: true,
      allowRemoteDataSearch: true,
      closeDropDownOnSelection: true,
      clearSearchFilter: false
    };

    this.filmographyTypeDropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };

    this.personService.getSearchResults().subscribe(people => {
      this.people = people;
    });
  }

  searchFocus() {
    setTimeout(() => {
      const input = this.el.nativeElement.querySelector('.people-dropdown .filter-textbox > input');
      if (input) {
        input.focus();
      }
    }, 0);
  }

  onPeopleFilterChange($event: any): void {
    if ($event.length >= 2) {
      this.personService.getPeople($event).subscribe(data => {
        this.people = data;
      });
    } else {
      this.people = [];
    }
  }

  loadGenres(): void {
    this.genreService.getGenres().subscribe(data => {
      this.genres = data;
    });
  }

  loadFilmographyTypes(): void {
    this.filmographyTypeService.getFilmographyTypes().subscribe(data => {
      this.filmographyTypes = data;
    });
  }

  onGenreSelect(): void {
    this.movieForm.get('genres')?.setValue(this.editedMovie.genres);
    this.movieForm.get('genres')?.markAsTouched();
    this.movieForm.get('genres')?.updateValueAndValidity();
  }

  onGenreTouched(): void {
    this.movieForm.get('genres')?.markAsTouched();
    this.movieForm.get('genres')?.updateValueAndValidity();
  }

  onPersonSelected(event: any): void {
    this.selectedPerson = event;
  }

  onFilmographyTypeSelected(event: any): void {
    this.selectedFilmographyType = event;
  }

  addFilmography(): void {
    if (this.selectedPerson && this.selectedFilmographyType) {
      if (!this.editedMovie.filmographies) {
        this.editedMovie.filmographies = [];
      }

      if (!this.editedMovie.filmographies.some(f => f.person?.id === this.selectedPerson?.id && f.filmographyType?.id === this.selectedFilmographyType?.id)) {
        this.editedMovie.filmographies.push({
          person: this.selectedPerson,
          filmographyType: this.selectedFilmographyType
        });
      } else {
        const toast = {
          textOrTpl: 'Person and role already present.',
          classname: 'bg-warning text-dark',
          delay: 3000
        };
        this.toastService.show(toast.textOrTpl, toast, 'center');
        this.toastService.removeAfterDelay(toast, toast.delay);
      }
    } else {
      const toast = {
        textOrTpl: 'Please select a person and a filmography type before adding.',
        classname: 'bg-warning text-dark',
        delay: 3000
      };
      this.toastService.show(toast.textOrTpl, toast, 'center');
      this.toastService.removeAfterDelay(toast, toast.delay);
    }
  }

  removeFilmography(filmography: Filmography): void {
    if (this.editedMovie.filmographies) {
      this.editedMovie.filmographies = this.editedMovie.filmographies.filter(f => f.personId !== filmography.personId || f.filmographyTypeId !== filmography.filmographyTypeId);
    }
  }

  saveMovie(): void {
    this.showValidation = true;
    Object.keys(this.movieForm.controls).forEach(field => {
      const control = this.movieForm.get(field);
      control?.markAsTouched({ onlySelf: true });
    });

    if (this.movieForm.invalid || this.editedMovie.genres.length === 0) {
      return;
    }

    // Convert the releaseDate to ISO format
    const movieToSave = {
      ...this.editedMovie,
      ...this.movieForm.value,
      releaseDate: convertNgbDateToISO(this.movieForm.value.releaseDate),
      filmographies: this.editedMovie.filmographies
    };

    if (this.isEditMode) {
      this.movieService.updateMovie(movieToSave).subscribe(() => {
        this.movieService.notifyMovieUpdated();
        this.activeModal.close('saved');
        this.router.navigate(['movies', movieToSave.id]);
      });
    } else {
      this.movieService.createMovie(movieToSave).subscribe(createdMovie => {
        this.movieService.notifyMovieUpdated();
        this.activeModal.close('saved');
        this.router.navigate(['movies', createdMovie.id]);
      });
    }
  }
}
