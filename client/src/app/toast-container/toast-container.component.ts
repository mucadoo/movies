import { Component } from '@angular/core';
import { ToastService } from '../services/toast.service';
import {NgbToast} from '@ng-bootstrap/ng-bootstrap';
import {NgClass, NgForOf} from '@angular/common';

@Component({
  selector: 'app-toast-container',
  templateUrl: './toast-container.component.html',
  imports: [
    NgbToast,
    NgClass,
    NgForOf
  ],
  styleUrls: ['./toast-container.component.scss']
})
export class ToastContainerComponent {
  constructor(public toastService: ToastService) {}

  getPositionClass(position: string) {
    switch (position) {
      case 'top-right':
        return 'position-absolute top-0 end-0';
      case 'bottom-right':
        return 'position-absolute bottom-0 end-0';
      case 'top-left':
        return 'position-absolute top-0 start-0';
      case 'bottom-left':
        return 'position-absolute bottom-0 start-0';
      case 'center':
        return 'position-fixed top-50 start-50 translate-middle';
      default:
        return 'position-absolute top-0 end-0'; // Default to top-right
    }
  }

}
