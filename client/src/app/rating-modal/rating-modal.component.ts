import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModule, NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, NgForm } from '@angular/forms';
import { VoteService } from '../services/vote.service';
import {Vote} from '../model/vote.model';
import {NgClass, NgIf} from '@angular/common';

@Component({
  selector: 'app-rating-modal',
  imports: [
    NgbModule,
    FormsModule,
    NgIf,
    NgClass,
  ],
  templateUrl: './rating-modal.component.html',
  styleUrls: ['./rating-modal.component.scss']
})
export class RatingModalComponent implements OnInit {
  @Input() movieId: number = 0;
  @Input() movieName: string = '';
  @Output() ratingSubmitted = new EventEmitter<void>();
  email: string = '';
  rating: number = 0;
  ratingInvalid: boolean = false;
  buttonText: string = 'Submit';

  constructor(public activeModal: NgbActiveModal, ratingConfig: NgbRatingConfig, private voteService: VoteService) {
    ratingConfig.max = 10;
    ratingConfig.readonly = false;
  }

  ngOnInit(): void {}

  onEmailChange(emailInput: any): void {
    if (emailInput.valid) {
      this.voteService.getVotesByMovie(String(this.movieId)).subscribe(votes => {
        if (votes.some(vote => vote.email === this.email)) {
          this.buttonText = 'Update';
        } else {
          this.buttonText = 'Submit';
        }
      });
    }
  }

  onRatingChange(rate: number): void {
    this.rating = rate;
    this.ratingInvalid = this.rating === 0;
  }

  onSubmit(form: NgForm): void {
    this.ratingInvalid = this.rating === 0;
    if (form.valid && !this.ratingInvalid) {
      const vote: Vote = {
        email: this.email,
        grade: this.rating,
        movieId: this.movieId
      };
      this.voteService.addVote(vote).subscribe(() => {
        this.ratingSubmitted.emit();
        this.activeModal.close();
      });
    } else {
      form.controls['email'].markAsTouched();
    }
  }
}
