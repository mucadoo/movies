import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {NgForOf} from '@angular/common';
import {MovieService} from '../services/movie.service';
import {Movie} from '../model/movie.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MovieModalComponent} from '../movie-modal/movie-modal.component';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
  imports: [
    FormsModule,
    NgForOf
  ]
})
export class MovieListComponent implements OnInit {
  searchMovie: string = '';
  movies: Movie[] = [];
  selectedMovieId: string | null = null;

  constructor(private movieService: MovieService, private router: Router, private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.updateMovieList();
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.selectedMovieId = this.getCurrentMovieId();
      }
    });

    this.movieService.movieUpdated$.subscribe(() => {
      this.updateMovieList();
    });
  }

  updateMovieList(): void {
    this.movieService.getMovies(this.searchMovie).subscribe(data => {
      this.movies = data;
      this.selectedMovieId = this.getCurrentMovieId();
    });
  }

  showMovie(movie: any): void {
    this.router.navigate(['movies', movie.id]);
  }

  getCurrentMovieId(): string | null {
    const currentUrl = this.router.routerState.snapshot.url;
    const urlParts = currentUrl.split('/');
    return currentUrl.startsWith('/movies/') && urlParts.length === 3 ? urlParts[2] : null;
  }

  isSelected(movie: Movie): boolean {
    return this.selectedMovieId != null && movie.id != null && this.selectedMovieId === movie.id.toString();
  }

  addMovie(): void {
    const modalRef = this.modalService.open(MovieModalComponent, { size: 'xl' });
    modalRef.componentInstance.isEditMode = false;
    modalRef.componentInstance.movie = new Movie();
  }
}
