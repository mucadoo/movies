import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PersonService } from '../services/person.service';
import { MovieService } from '../services/movie.service';
import { FilmographyTypeService } from '../services/filmography-type.service';
import { Router } from '@angular/router';
import { Person } from '../model/person.model';
import { Movie } from '../model/movie.model';
import { FilmographyType } from '../model/filmography-type.model';
import { NgClass, NgForOf, NgIf } from '@angular/common';
import { ToastService } from '../services/toast.service';
import {Filmography} from '../model/filmography.model';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';

function formatNgbDateStruct(dateString: string): { year: number, month: number, day: number } {
  const date = new Date(dateString);
  return {
    year: date.getFullYear(),
    month: date.getMonth() + 1,
    day: date.getDate()
  };
}

function deepCopyAndFormat<T>(object: T): T {
  const copy = JSON.parse(JSON.stringify(object));
  if (copy && copy.born) {
    copy.born = formatNgbDateStruct(copy.born);
  }
  return copy;
}

function convertNgbDateToISO(ngbDate: { year: number, month: number, day: number }): string {
  const date = new Date(Date.UTC(ngbDate.year, ngbDate.month - 1, ngbDate.day));
  return date.toISOString();
}

@Component({
  selector: 'app-person-modal',
  templateUrl: './person-modal.component.html',
  imports: [
    ReactiveFormsModule,
    NgClass,
    FormsModule,
    NgIf,
    NgForOf,
    NgbInputDatepicker,
    NgMultiSelectDropDownModule
  ],
  styleUrls: ['./person-modal.component.scss']
})
export class PersonModalComponent implements OnInit {
  @Input() isEditMode: boolean = false;
  @Input() person: Person = new Person();
  editedPerson: Person = new Person();
  filmographyTypes: FilmographyType[] = [];
  movies: Movie[] = [];
  selectedMovie: Movie | null = null;
  selectedFilmographyType: FilmographyType | null = null;
  personForm: FormGroup;
  showValidation: boolean = false;
  movieDropdownSettings = {};
  filmographyTypeDropdownSettings = {};

  constructor(
    public activeModal: NgbActiveModal,
    private personService: PersonService,
    private movieService: MovieService,
    private filmographyTypeService: FilmographyTypeService,
    private fb: FormBuilder,
    private router: Router,
    protected toastService: ToastService,
    private el: ElementRef
  ) {
    this.personForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      born: ['', Validators.required],
      posterLink: ['', [Validators.required, Validators.pattern(/^(ftp|http|https):\/\/[^ "]+$/)]],
      resume: ['', [Validators.required, Validators.maxLength(1000)]]
    });
  }

  ngOnInit(): void {
    // Create a deep copy of the person object for editing
    this.editedPerson = deepCopyAndFormat(this.person);

    // Load filmography types
    this.loadFilmographyTypes();

    // Patch the person form with the edited person data
    this.personForm.patchValue(this.editedPerson);

    this.movieDropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      allowSearchFilter: true,
      allowRemoteDataSearch: true,
      closeDropDownOnSelection: true,
      clearSearchFilter: false
    };

    this.filmographyTypeDropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };
  }

  loadFilmographyTypes(): void {
    this.filmographyTypeService.getFilmographyTypes().subscribe(data => {
      this.filmographyTypes = data;
    });
  }

  searchMovies(query: any): void {
    if (query.length >= 2) {
      this.movieService.getMovies(query).subscribe(data => {
        this.movies = data;
      });
    } else {
      this.movies = [];
    }
  }

  onMovieSelected(event: any): void {
    this.selectedMovie = event;
  }

  onFilmographyTypeSelected(event: any): void {
    this.selectedFilmographyType = event;
  }

  addFilmography(): void {
    if (this.selectedMovie && this.selectedFilmographyType) {
      if (!this.editedPerson.filmographies) {
        this.editedPerson.filmographies = [];
      }

      if (!this.editedPerson.filmographies.some(f => f.movie?.id === this.selectedMovie?.id && f.filmographyType?.id === this.selectedFilmographyType?.id)) {
        this.editedPerson.filmographies.push({
          movie: this.selectedMovie,
          filmographyType: this.selectedFilmographyType
        });
      } else {
        const toast = {
          textOrTpl: 'Movie and role already present.',
          classname: 'bg-warning text-dark',
          delay: 3000
        };
        this.toastService.show(toast.textOrTpl, toast, 'center');
        this.toastService.removeAfterDelay(toast, toast.delay);
      }
    } else {
      const toast = {
        textOrTpl: 'Please select a movie and a filmography type before adding.',
        classname: 'bg-warning text-dark',
        delay: 3000
      };
      this.toastService.show(toast.textOrTpl, toast, 'center');
      this.toastService.removeAfterDelay(toast, toast.delay);
    }
  }

  removeFilmography(filmography: Filmography): void {
    if (this.editedPerson.filmographies) {
      this.editedPerson.filmographies = this.editedPerson.filmographies.filter(f => f.movieId !== filmography.movieId || f.filmographyTypeId !== filmography.filmographyTypeId);
    }
  }

  searchFocus() {
    setTimeout(() => {
      const input = this.el.nativeElement.querySelector('.people-dropdown .filter-textbox > input');
      if (input) {
        input.focus();
      }
    }, 0);
  }

  savePerson(): void {
    this.showValidation = true;
    Object.keys(this.personForm.controls).forEach(field => {
      const control = this.personForm.get(field);
      control?.markAsTouched({ onlySelf: true });
    });

    if (this.personForm.invalid) {
      return;
    }

    // Convert the date to ISO format
    const personToSave = {
      ...this.editedPerson,
      ...this.personForm.value,
      born: convertNgbDateToISO(this.personForm.value.born)
    };

    if (this.isEditMode) {
      this.personService.updatePerson(personToSave).subscribe(() => {
        this.personService.notifyPersonUpdated();
        this.activeModal.close('saved');
        this.router.navigate(['people', personToSave.id]);
      });
    } else {
      this.personService.createPerson(personToSave).subscribe(createdPerson => {
        this.personService.notifyPersonUpdated();
        this.activeModal.close('saved');
        this.router.navigate(['people', createdPerson.id]);
      });
    }
  }
}
