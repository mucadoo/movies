import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Person } from '../model/person.model';
import { Filmography } from '../model/filmography.model';
import { PersonService } from '../services/person.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { DatePipe, NgForOf, NgIf } from '@angular/common';
import { PersonModalComponent } from '../person-modal/person-modal.component';
import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';

@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.scss'],
  imports: [
    NgForOf,
    NgIf,
    DatePipe
  ]
})
export class PersonDetailComponent implements OnInit, OnChanges {
  @Input() personId: string | null = null;
  selectedPerson: Person | undefined;
  activeTab: number = 0;

  constructor(
    private personService: PersonService,
    private modalService: NgbModal,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadPersonDetail();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['personId'] && !changes['personId'].isFirstChange()) {
      this.loadPersonDetail();
    }
  }

  private loadPersonDetail(): void {
    if (this.personId) {
      this.personService.getPerson(String(this.personId)).subscribe(data => {
        this.selectedPerson = data;
      });
    }
  }

  getUniqueFilmographyTypes(): string[] {
    if (!this.selectedPerson || !this.selectedPerson.filmographies) {
      return [];
    }
    const uniqueTypes = new Set<string>();
    this.selectedPerson.filmographies.forEach(f => {
      if (f.filmographyType) {
        uniqueTypes.add(f.filmographyType.name);
      }
    });
    return Array.from(uniqueTypes);
  }

  getFilmographiesByType(typeName: string): Filmography[] {
    if (!this.selectedPerson || !this.selectedPerson.filmographies) {
      return [];
    }
    return this.selectedPerson.filmographies.filter(
      filmography => filmography.filmographyType?.name === typeName
    );
  }

  editPerson(editMode: boolean, person: Person = new Person()): void {
    const modalRef = this.modalService.open(PersonModalComponent, { size: 'xl' });
    modalRef.componentInstance.isEditMode = editMode;
    modalRef.componentInstance.person = person;

    modalRef.result.then((result) => {
      if (result === 'saved') {
        this.loadPersonDetail();
      }
    }).catch((error) => {
      console.log(error);
    });
  }

  navigateToMovie(movieId: number | undefined) {
    this.router.navigate([`/movies/${movieId}`]);
  }

  deletePerson(person: Person): void {
    const modalRef = this.modalService.open(ConfirmationModalComponent);
    modalRef.componentInstance.message = `Are you sure you want to delete ${person.name}?`;

    modalRef.result.then((result) => {
      if (result === 'confirm') {
        this.personService.deletePerson(String(person.id)).subscribe(() => {
          this.router.navigate(['/people']);
        });
      }
    }).catch((error) => {
      console.log(error);
    });
  }

}
