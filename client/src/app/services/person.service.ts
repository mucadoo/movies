import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, catchError } from 'rxjs/operators';
import { Person } from '../model/person.model';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private apiUrl = 'http://localhost:3000/api/people';
  private searchTerms = new Subject<string>();
  private personUpdatedSubject = new Subject<void>();

  constructor(private http: HttpClient) {
    this.setupSearch();
  }

  search(term: string): void {
    this.searchTerms.next(term);
  }

  private setupSearch(): void {
    this.searchTerms.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      switchMap(term => this.getPeople(term))
    ).subscribe();
  }

  getPeople(searchTerm: string): Observable<Person[]> {
    return this.http.get<Person[]>(`${this.apiUrl}?query=${searchTerm}`).pipe(
      catchError(() => of([]))
    );
  }

  getSearchResults(): Observable<Person[]> {
    return this.searchTerms.asObservable().pipe(
      switchMap(term => this.getPeople(term))
    );
  }

  getPerson(id: string): Observable<Person> {
    return this.http.get<Person>(`${this.apiUrl}/${id}`);
  }

  createPerson(person: Person): Observable<Person> {
    return this.http.post<Person>(this.apiUrl, person);
  }

  updatePerson(person: Person): Observable<Person> {
    return this.http.put<Person>(`${this.apiUrl}/${person.id}`, person);
  }

  deletePerson(id: string): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/${id}`);
  }

  notifyPersonUpdated(): void {
    this.personUpdatedSubject.next();
  }

  get personUpdated$(): Observable<void> {
    return this.personUpdatedSubject.asObservable();
  }
}
