import { Injectable, TemplateRef } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ToastService {
  toasts: any[] = [];

  show(textOrTpl: string | TemplateRef<any>, options: any = {}, position: string = 'top-right') {
    this.toasts.push({ textOrTpl, ...options, position });
  }

  remove(toast: any) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }

  removeAfterDelay(toast: any, delay: number) {
    setTimeout(() => this.remove(toast), delay);
  }
}
