import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FilmographyType } from '../model/filmography-type.model';

@Injectable({
  providedIn: 'root'
})
export class FilmographyTypeService {
  private baseUrl = 'http://localhost:3000/api/filmography-types';

  constructor(private http: HttpClient) {}

  getFilmographyTypes(): Observable<FilmographyType[]> {
    return this.http.get<FilmographyType[]>(this.baseUrl);
  }
}
