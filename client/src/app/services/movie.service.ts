import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, catchError } from 'rxjs/operators';
import { Movie } from '../model/movie.model';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private apiUrl = 'http://localhost:3000/api/movies';
  private movieUpdatedSubject = new Subject<void>();
  private searchTerms = new Subject<string>();

  constructor(private http: HttpClient) {
    this.setupSearch();
  }

  search(term: string): void {
    this.searchTerms.next(term);
  }

  private setupSearch(): void {
    this.searchTerms.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      switchMap(term => this.getMovies(term))
    ).subscribe();
  }

  getMovies(searchTerm: string): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${this.apiUrl}?query=${searchTerm}`).pipe(
      catchError(() => of([]))
    );
  }

  getMovie(id: string): Observable<Movie> {
    return this.http.get<Movie>(`${this.apiUrl}/${id}`);
  }

  createMovie(movie: Movie): Observable<Movie> {
    return this.http.post<Movie>(this.apiUrl, movie);
  }

  updateMovie(movie: Movie): Observable<Movie> {
    return this.http.put<Movie>(`${this.apiUrl}/${movie.id}`, movie);
  }

  deleteMovie(id: string): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/${id}`);
  }

  notifyMovieUpdated(): void {
    this.movieUpdatedSubject.next();
  }

  get movieUpdated$(): Observable<void> {
    return this.movieUpdatedSubject.asObservable();
  }
}
