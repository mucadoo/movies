import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { LoaderService } from './loader.service';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

  constructor(private loaderService: LoaderService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const MINIMUM_LOADER_TIME = 500;
    const start = Date.now();

    this.loaderService.show();
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        return throwError(() => new Error(error.message));
      }),
      finalize(() => {
        const elapsed = Date.now() - start;
        const remaining = MINIMUM_LOADER_TIME - elapsed;

        if (remaining > 0) {
          setTimeout(() => this.loaderService.hide(), remaining);
        } else {
          this.loaderService.hide();
        }
      })
    );
  }
}
