import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Vote} from '../model/vote.model';

@Injectable({
  providedIn: 'root'
})
export class VoteService {
  private baseUrl = 'http://localhost:3000/api/votes';

  constructor(private http: HttpClient) {}

  getVotesByMovie(movieId: string): Observable<Vote[]> {
    return this.http.get<Vote[]>(`${this.baseUrl}/movie/${movieId}`);
  }

  addVote(vote: Vote): Observable<Vote> {
    return this.http.post<Vote>(this.baseUrl, vote);
  }
}
