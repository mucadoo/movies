# Use the official Node.js image as the base image
FROM node:22

# Install netcat-openbsd and PostgreSQL client
RUN apt-get update && apt-get install -y netcat-openbsd postgresql-client

# Set the working directory inside the container to /usr/src/app/server
WORKDIR /usr/src/app/server

# Copy the package.json and package-lock.json files from the server folder
COPY server/package*.json ./

# Install the dependencies
RUN npm install

# Copy the rest of the server application code
COPY server ./

# Build the TypeScript code
RUN npm run build

# Expose the port the app runs on
EXPOSE $APP_PORT

# Add entrypoint script
COPY server/docker-entrypoint.sh /usr/src/app/server/

# Make entrypoint script executable
RUN chmod +x /usr/src/app/server/docker-entrypoint.sh

# Command to run the entrypoint script
ENTRYPOINT ["./docker-entrypoint.sh"]
